import requests


def allowed(identifier: bytes):
    response = requests.get("https://apis.rindula.de/chat_access/check.php", params={
        'hash': identifier.decode()
    })
    r = response.text.strip()
    if r != "":
        return r
    return False
