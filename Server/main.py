import base64
import select
import socket
import subprocess
import threading

import functions

HEADER_LENGTH = 10

IP = "0.0.0.0"
PORT = 9999

# Create a socket
# socket.AF_INET - address family, IPv4, some otehr possible are AF_INET6, AF_BLUETOOTH, AF_UNIX
# socket.SOCK_STREAM - TCP, conection-based, socket.SOCK_DGRAM - UDP, connectionless, datagrams, socket.SOCK_RAW - raw IP packets
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# SO_ - socket option
# SOL_ - socket option level
# Sets REUSEADDR (as a socket option) to 1 on socket
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind, so server informs operating system that it's going to use given IP and port
# For a server using 0.0.0.0 means to listen on all available interfaces, useful to connect locally to 127.0.0.1 and remotely to LAN interface IP
server_socket.bind((IP, PORT))

# This makes server listen to new connections
server_socket.listen()

# List of sockets for select.select()
sockets_list = [server_socket]

# List of connected clients - socket as a key, user header and name as data
clients = {}

print(f'Listening for connections on {IP}:{PORT}...')


# Handles message receiving
def receive_message(client_socket):
    try:

        # Receive our "header" containing message length, it's size is defined and constant
        message_header = client_socket.recv(HEADER_LENGTH)

        # If we received no data, client gracefully closed a connection, for example using socket.close() or socket.shutdown(socket.SHUT_RDWR)
        if not len(message_header):
            return False

        # Convert header to int value
        message_length = int(message_header.decode('utf-8').strip())

        # Return an object of message header and message data
        return {'header': message_header, 'data': client_socket.recv(message_length)}

    except:

        # If we are here, client closed connection violently, for example by pressing ctrl+c on his script
        # or just lost his connection
        # socket.close() also invokes socket.shutdown(socket.SHUT_RDWR) what sends information about closing the socket (shutdown read/write)
        # and that's also a cause when we receive an empty message
        return False


def runCommand(command: str):
    print("Running Command '{}'".format(command))
    proc = subprocess.Popen(command, stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    print("Returning Command '{}'".format(command))
    return out, err


def header_length(msg: bytes) -> bytes:
    return bytes(f"{len(msg.decode('utf-8')):<{HEADER_LENGTH}}", encoding='utf-8')


def messified(param: bytes) -> bytes:
    return header_length(base64.encodebytes(param)) + base64.encodebytes(param)
    pass


def prefix(param: str) -> bytes:
    return bytes(f"{param:<{HEADER_LENGTH}}", encoding='utf-8')
    pass


def send(cs, cmd):
    print(f"Sending {cmd}")
    cs.send(cmd)
    pass


def commandInterpreter(netcmd: str) -> bool:
    vars = netcmd.split(" ")
    cmd = vars[0]
    host = vars[1]
    if cmd == "target":
        cmd = bytes("mkdir ~/targets/{0}/".format(host), encoding='utf-8')
        send(client_socket, prefix("CMD") + messified(b"Server") + messified(cmd))
        commandInterpreter("recon %s" % host)
        return True
    if cmd == "recon":
        o, e = runCommand("nmap -sC -sV -vvv %s" % host)
        send(client_socket, prefix("CMD") + messified(b"Server") + messified(bytes("mkdir ~/targets/{0}/nmap".format(host), encoding='utf-8')))
        send(client_socket, prefix("FILE") + messified(bytes("~/targets/{0}/nmap/initial.log".format(host), encoding='utf-8')) + messified(o))
        send(client_socket, prefix("COMMAND") + messified(bytes(netcmd, encoding='utf-8')) + messified(e) + messified(o))
        threading.Thread(target=commandInterpreter,
                         args=("reconExessive %s" % host,)).start()
        if "80/tcp   open" in o.decode('utf-8', errors='ignore'):
            commandInterpreter("bust %s" % host)
        return True
    if cmd == "reconExessive":
        o, e = runCommand("nmap -p- -A -sC -sV -vvv %s" % host)
        send(client_socket, prefix("CMD") + messified(b"Server") + messified(bytes("mkdir ~/targets/{0}/nmap".format(host), encoding='utf-8')))
        send(client_socket, prefix("FILE") + messified(bytes("~/targets/{0}/nmap/exessive.log".format(host), encoding='utf-8')) + messified(o))
        send(client_socket, prefix("COMMAND") + messified(bytes(netcmd, encoding='utf-8')) + messified(e) + messified(o))
        return True
    if cmd == "bust":
        o, e = runCommand(
            "gobuster -u {0} -x php,html,txt,phtml,xml,htm -w /usr/share/wordlists/dirbuster/directory-list-".format(
                host))
        send(client_socket,
             prefix("FILE") + messified(bytes("~/targets/{0}/gobuster.txt".format(host), encoding='utf-8')) + messified(
                 o))
        send(client_socket,
             prefix("COMMAND") + messified(bytes(netcmd, encoding='utf-8')) + messified(e) + messified(o))
    if cmd == "crack":
        #crunch 8 8 | john --stdin --session=superwifi --stdout
        pass

    return False
    pass


while True:

    # Calls Unix select() system call or Windows select() WinSock call with three parameters:
    #   - rlist - sockets to be monitored for incoming data
    #   - wlist - sockets for data to be send to (checks if for example buffers are not full and socket is ready to send some data)
    #   - xlist - sockets to be monitored for exceptions (we want to monitor all sockets for errors, so we can use rlist)
    # Returns lists:
    #   - reading - sockets we received some data on (that way we don't have to check sockets manually)
    #   - writing - sockets ready for data to be send thru them
    #   - errors  - sockets with some exceptions
    # This is a blocking call, code execution will "wait" here and "get" notified in case any action should be taken
    read_sockets, _, exception_sockets = select.select(sockets_list, [], sockets_list)

    # Iterate over notified sockets
    for notified_socket in read_sockets:

        # If notified socket is a server socket - new connection, accept it
        if notified_socket == server_socket:

            # Accept new connection
            # That gives us new socket - client socket, connected to this given client only, it's unique for that client
            # The other returned object is ip/port set
            client_socket, client_address = server_socket.accept()

            # Client should send his name right away, receive it
            user = receive_message(client_socket)

            # If False - client disconnected before he sent his name
            if user is False:
                continue

            permitted = functions.allowed(user['data'])
            if not permitted:
                client_socket.close()
                continue
            user['alias'] = bytes(permitted.strip(), encoding='utf-8')
            user['aliasheader'] = bytes(f"{len(user['alias']):<{HEADER_LENGTH}}", encoding='utf-8')
            # Add accepted socket to select.select() list
            sockets_list.append(client_socket)

            # Also save username and username header
            clients[client_socket] = user

            print('Accepted new connection from {}:{}, user: {}'.format(*client_address, user['alias']))

        # Else existing socket is sending a message
        else:

            # Receive type
            types = receive_message(notified_socket)
            # Receive message
            message = receive_message(notified_socket)

            # If False, client disconnected, cleanup
            if message is False:
                print('Closed connection from: {}'.format(
                    clients[notified_socket]['alias'].decode('utf-8')))

                # Remove from list for socket.socket()
                sockets_list.remove(notified_socket)

                # Remove from our list of users
                del clients[notified_socket]

                continue

            # Get user by notified socket, so we will know who sent the message
            user = clients[notified_socket]

            print(
                f'Received message from {user["alias"].decode()}: {base64.decodebytes(message["data"]).decode("utf-8")}')

            # Iterate over connected clients and broadcast message
            for client_socket in clients:
                if types['data'] == b"COMMAND":
                    rcmsg = "Running Command '{}'".format(base64.decodebytes(message['data']).decode('utf-8').strip())
                    send(client_socket,
                        prefix("MESSAGE") + messified(user['alias']) + messified(bytes(rcmsg, encoding='utf-8')))
                    threading.Thread(target=commandInterpreter,
                                     args=(base64.decodebytes(message['data']).decode('utf-8').strip(),)).start()
                elif types['data'] == b"MESSAGE":
                    send(client_socket,
                        prefix("MESSAGE") + messified(user['alias'].strip()) + message['header'] + message['data'])

    # It's not really necessary to have this, but will handle some socket exceptions just in case
    for notified_socket in exception_sockets:
        # Remove from list for socket.socket()
        sockets_list.remove(notified_socket)

        # Remove from our list of users
        del clients[notified_socket]
