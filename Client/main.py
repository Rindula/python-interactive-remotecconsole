import base64
import datetime
import errno
import os
import socket
import threading
from tkinter import *

import credentials

HEADER_LENGTH = 10

IP = "127.0.0.1"
PORT = 9999

# Create a socket
# socket.AF_INET - address family, IPv4, some otehr possible are AF_INET6, AF_BLUETOOTH, AF_UNIX
# socket.SOCK_STREAM - TCP, conection-based, socket.SOCK_DGRAM - UDP, connectionless, datagrams, socket.SOCK_RAW - raw IP packets
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to a given ip and port
client_socket.connect((IP, PORT))

# Set connection to non-blocking state, so .recv() call won;t block, just return some exception we'll handle
client_socket.setblocking(False)

# Prepare username and header and send them
# We need to encode username to bytes, then count number of bytes and prepare header of fixed size, that we encode to bytes as well
username_header = f"{len(credentials.identifier):<{HEADER_LENGTH}}".encode('utf-8')
client_socket.send(username_header + credentials.identifier)


def windowsify(path: str) -> str:
    path = path.replace("~", os.environ['USERPROFILE'])
    path = path.replace("/", "\\")
    return path


def receive(tb: Text):
    while True:
        try:
            # Now we want to loop over received messages (there might be more than one) and print them
            while True:

                # Receive our "header" containing username length, it's size is defined and constant
                type = client_socket.recv(HEADER_LENGTH).decode('utf-8').strip()
                username_header = client_socket.recv(HEADER_LENGTH)
                # If we received no data, server gracefully closed a connection, for example using socket.close() or socket.shutdown(socket.SHUT_RDWR)
                if not len(username_header):
                    print('Connection closed by the server')
                    sys.exit()

                # Convert header to int value
                username_length = int(username_header.decode('utf-8').strip())

                # Receive and decode username
                username = base64.decodebytes(client_socket.recv(username_length)).decode('utf-8')

                if type == "COMMAND":
                    error_header = client_socket.recv(HEADER_LENGTH)
                    error_length = int(error_header.decode('utf-8').strip())
                    error = base64.decodebytes(client_socket.recv(error_length)).decode(encoding='utf-8',
                                                                                        errors='ignore')

                # Now do the same for message (as we received username, we received whole message, there's no need to check if it has any length)
                message_header = client_socket.recv(HEADER_LENGTH)
                message_length = int(message_header.decode('utf-8').strip())
                message = base64.decodebytes(client_socket.recv(message_length)).decode(encoding='utf-8',
                                                                                        errors='ignore')

                # Print message
                if type == "COMMAND":
                    print(f'$ {username}\n{message}')
                    tb.insert(END, message + "\n")
                    if error_length > 0:
                        print(f"=======\nERRORS:\n=======\n{error}")
                        filename = "errors.log"
                        if os.path.exists(filename):
                            append_write = 'a'  # append if already exists
                        else:
                            append_write = 'w'  # make a new file if not

                        with open(filename, append_write) as file:
                            if append_write == "a":
                                file.write("\n")
                            file.write(f"{username}\n\n{error}\n")
                            file.close()
                if type == "CMD":
                    if os.name == 'nt':
                        message = windowsify(message)
                    os.system(message)
                if type == "FILE":
                    if os.name == 'nt':
                        username = windowsify(username)
                    with open(username, 'w', errors='ignore') as file:
                        file.write(message.replace("\r", ""))
                        file.close()
                if type == "MESSAGE":
                    print(f'[{datetime.datetime.now().strftime("%H:%M:%S")}] <{username}> {message}')
                    tb.insert(END, f'[{datetime.datetime.now().strftime("%H:%M:%S")}] <{username}> {message}\n')

        except IOError as e:
            # This is normal on non blocking connections - when there are no incoming data error is going to be raised
            # Some operating systems will indicate that using AGAIN, and some using WOULDBLOCK error code
            # We are going to check for both - if one of them - that's expected, means no incoming data, continue as normal
            # If we got different error code - something happened
            if e.errno != errno.EAGAIN and e.errno != errno.EWOULDBLOCK:
                print('Reading error: {}'.format(e))
                sys.exit()

            # We just did not receive anything
            continue

        except Exception as e:
            # Any other exception - something happened, exit
            print('Reading error: '.format(e))


def main():
    window = Tk()
    window.resizable = False

    messages = Text(window)
    messages.pack()

    input_user = StringVar()
    input_field = Entry(window, text=input_user)
    input_field.pack(side=BOTTOM, fill=X)

    threading.Thread(target=receive, args=(messages,)).start()

    def enter_pressed(event):
        message = input_field.get()
        if message:
            if message.startswith("!"):
                message = base64.encodebytes(message[1:].encode('utf-8'))
                message_header = f"{len(message):<{HEADER_LENGTH}}".encode('utf-8')
                types = b"COMMAND"
                types_header = f"{len(types):<{HEADER_LENGTH}}".encode('utf-8')
                client_socket.send(types_header + types + message_header + message)
            else:
                message = base64.encodebytes(message.encode('utf-8'))
                message_header = f"{len(message):<{HEADER_LENGTH}}".encode('utf-8')
                types = b"MESSAGE"
                types_header = f"{len(types):<{HEADER_LENGTH}}".encode('utf-8')
                client_socket.send(types_header + types + message_header + message)

        input_user.set('')
        return "break"

    frame = Frame(window)  # , width=300, height=300)
    input_field.bind("<Return>", enter_pressed)
    frame.pack()

    window.mainloop()


if __name__ == '__main__':
    main()
